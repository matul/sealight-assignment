class QueueExists extends Error {
    constructor(queueName: string) {
        super(`Queue ${queueName} already exists`);
    }
}