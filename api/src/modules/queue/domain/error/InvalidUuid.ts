export class InvalidUuid extends Error {
    constructor(invalid: string) {
        super(`${invalid} is not a valid Uuid`);
    }
}
