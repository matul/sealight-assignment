export class NonExistentQueue extends Error {
    constructor(queueName: string) {
        super(`Queue named ${queueName} does not exist`);
    }
}
