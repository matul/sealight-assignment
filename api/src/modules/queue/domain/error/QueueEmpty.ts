export class QueueEmpty extends Error {
    constructor(queueName: string) {
        super(`queue ${queueName} is empty`);
    }
}
