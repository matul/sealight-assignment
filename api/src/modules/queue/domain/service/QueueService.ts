import { Inject } from '@nestjs/common';
import { Queue } from '../aggregate';
import { NonExistentQueue } from '../error/NonExistentQueue';
import { IQueueRepository } from '../repository/IQueueRepository';

export class QueueService {
    private queues: Queue[] = [];

    constructor(
        @Inject('QUEUE_REPOSITORY') private repository: IQueueRepository,
    ) {}

    async createQueue(queueName: string) {
        if (this.queues.some(({ name }) => name === queueName)) throw new QueueExists(queueName);

        const newQueue = Queue.create(queueName);
        this.queues.push(newQueue);
        await this.repository.persistQueue(newQueue);
    }

    async deleteQueue(queueName: string) {
        if (!this.queues.some(({ name }) => name === queueName)) throw new NonExistentQueue(queueName);
        this.queues = this.queues.filter(({ name }) => name !== queueName);
        await this.repository.deleteQueue(queueName);
    }

    async enqueue(queueName: string, message: string) {
        const queue = this.getQueue(queueName);

        queue.enqueue(message);
        await this.repository.persistQueue(queue);
    }

    async dequeue(queueName: string) {
        const queue = this.getQueue(queueName);
        const message = queue.dequeue();

        await this.repository.removeMessage(message.id);

        return message;
    }

    async getSnapshot(queueName: string) {
        const queue = this.getQueue(queueName);

        return await queue.snapshot;
    }

    async loadQueues() {
        this.queues = await this.repository.getQueues();
    }

    private getQueue(queueName: string) {
        const queue = this.queues.find(({ name }) => name === queueName);
        if (!queue) throw new NonExistentQueue(queueName);

        return queue;
    }
}
