import { Queue } from '../aggregate';

export interface IQueueRepository {
    getQueues(): Promise<Queue[]>;
    persistQueue(queue: Queue): Promise<void>;
    removeMessage(messageId: string): Promise<void>;
    deleteQueue(queueName: string): Promise<void>;
}
