import { Message } from './Message';
import { Uuid } from './value-object/Uuid';

describe('Message entity', () => {
    it('is created from existing data', () => {
        const message = Message.createExisting(
            'foo',
            'd1bafbe5-5eee-415d-b8ff-2b30fe7743ee',
            new Date('2023-06-21'),
        );

        expect(message.id).toEqual(
            Uuid.createFrom('d1bafbe5-5eee-415d-b8ff-2b30fe7743ee'),
        );
        expect(message.contents).toEqual('foo');
        expect(message.createdAt).toEqual(new Date('2023-06-21'));
    });

    it('is created as new', () => {
        jest.useFakeTimers().setSystemTime(new Date('2023-06-20'));

        const message = Message.createNew('bar');

        expect(message.contents).toEqual('bar');
        expect(message.createdAt).toEqual(new Date('2023-06-20'));
    });
});
