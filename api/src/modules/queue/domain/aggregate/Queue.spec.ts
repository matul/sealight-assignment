import { validate } from 'uuid';
import { Message } from './Message';
import { Queue } from './Queue';
import { QueueEmpty } from '../error/QueueEmpty';

describe('Queue Aggregate', () => {
    let queue: Queue;

    beforeEach(() => {
        queue = Queue.create('foo');
    });

    it('is created with name', () => {
        expect(queue.name).toEqual('foo');
    });

    it('enqueues message', () => {
        jest.useFakeTimers().setSystemTime(new Date('2023-06-20'));
        queue.enqueue('foo');

        const snapshot = queue.snapshot;
        expect(snapshot.length).toEqual(1);
        const message = snapshot[0];

        expect(message).toEqual<Partial<typeof message>>(
            expect.objectContaining({
                createdAt: new Date('2023-06-20'),
                contents: 'foo',
            }),
        );

        expect(validate(message.id)).toBeTruthy();
    });

    it('enqueues message by object', () => {
        queue.enqueue(Message.createNew('bar'));

        const snapshot = queue.snapshot;
        expect(snapshot.length).toEqual(1);
        const message = snapshot[0];

        expect(message.contents).toEqual('bar');
    });

    it('dequeues message', () => {
        queue.enqueue('baz');
        const message = queue.dequeue();

        expect(message.contents).toEqual('baz');
        expect(queue.snapshot.length).toEqual(0);
    });

    it('throws when dequeueing empty', () => {
        expect(() => queue.dequeue()).toThrow(new QueueEmpty('foo'));
    });
});
