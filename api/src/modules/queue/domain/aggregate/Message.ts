import { Uuid } from './value-object/Uuid';

export class Message {
    private constructor(
        readonly id: Uuid,
        readonly contents: string,
        readonly createdAt: Date,
    ) {}

    static createNew(contents: string) {
        return new Message(Uuid.createNew(), contents, new Date(Date.now()));
    }
    static createExisting(contents: string, id: string, createdAt: Date) {
        return new Message(Uuid.createFrom(id), contents, createdAt);
    }
}
