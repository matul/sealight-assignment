import { QueueEmpty } from '../error/QueueEmpty';
import { Message } from './Message';

export class Queue {
    private messages: Message[] = [];
    private constructor(readonly name: string) {}

    enqueue(contents: string); // avoid violation of the Law of Demeter
    enqueue(message: Message); // allow to enqueue message object
    enqueue(message: string | Message) {
        const messageObj =
            message instanceof Message ? message : Message.createNew(message);

        this.messages.push(messageObj);
    }

    dequeue() {
        if (this.messages.length === 0) throw new QueueEmpty(this.name);
        return this.convertMessage(this.messages.shift());
    }

    get snapshot() {
        return this.messages.map(this.convertMessage);
    }

    private convertMessage(message: Message) {
        return {
            ...message, //convert message to clean object simultaneously creating a copy
            id: message.id.id, //unpack value object
        };
    }

    static create(name: string) {
        return new this(name);
    }
}
