import { v4 as createUuid, validate } from 'uuid';
import { InvalidUuid } from '../../error/InvalidUuid';

export class Uuid {
    private constructor(readonly id: string) {}

    static createNew() {
        return new this(createUuid());
    }

    static createFrom(existingUuid: string) {
        if (!validate(existingUuid))
            throw new InvalidUuid(`${existingUuid} is not a valid uuid`);

        return new this(existingUuid);
    }
}
