import { InvalidUuid } from '../../error/InvalidUuid';
import { Uuid } from './Uuid';
import { validate, v4 as newUuid } from 'uuid';

describe('Uuid value object', () => {
    it('creates brand new valid uuid', () => {
        const uuid = Uuid.createNew();
        expect(validate(uuid.id)).toBeTruthy();
    });

    it('creates uuid from existing id', () => {
        const existing = newUuid();
        const uuid = Uuid.createFrom(existing);

        expect(uuid.id).toEqual(existing);
    });

    it('throws when invalid id provided', () => {
        expect(() => Uuid.createFrom('bar')).toThrow(
            new InvalidUuid(`bar is not a valid uuid`),
        );
    });
});
