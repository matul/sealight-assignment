import {
    Body,
    ConflictException,
    Controller,
    Delete,
    Get,
    NotFoundException,
    Param,
    Post,
} from '@nestjs/common';
import { QueueService } from '../../domain/service/QueueService';
import { MessageDto } from '../dto/MessageDto';
import { NonExistentQueue } from '../../domain/error/NonExistentQueue';
import { QueueEmpty } from '../../domain/error/QueueEmpty';
import { QueueDto } from '../dto/QueueDto';

@Controller({
    path: '/queues',
})
export class QueueController {
    constructor(private queueService: QueueService) {}

    @Post('/')
    async createQueue(@Body() queueDto: QueueDto) {
        try {
            return await this.queueService.createQueue(queueDto.name);
        } catch (e) {
            if (e instanceof QueueExists) throw new ConflictException(e.message);
            throw e;
        }
    }

    @Delete('/:queueName')
    async deleteQueue(@Param('queueName') queueName: string) {
        try {
            return await this.queueService.deleteQueue(queueName);
        } catch (e) {
            if (e instanceof NonExistentQueue)
                throw new NotFoundException(e.message);

            throw e;
        }
    }

    @Get('/:queueName/messages')
    async getSnapshot(@Param('queueName') queueName: string) {
        try {
            return await this.queueService.getSnapshot(queueName);
        } catch (e) {
            if (e instanceof NonExistentQueue)
                throw new NotFoundException(e.message);

            throw e;
        }
    }
    @Post('/:queueName/messages')
    async enqueue(
        @Param('queueName') queueName: string,
        @Body() messageDto: MessageDto,
    ) {
        try {
            return await this.queueService.enqueue(
                queueName,
                messageDto.contents,
            );
        } catch (e) {
            if (e instanceof NonExistentQueue)
                throw new NotFoundException(e.message);

            throw e;
        }
    }

    /*
    Normally there should be message id in URL
    but since it is a queue we can get (and delete)
    only last message
    */
    @Delete('/:queueName/messages')
    async dequeue(@Param('queueName') queueName: string) {
        try {
            return await this.queueService.dequeue(queueName);
        } catch (e) {
            if (e instanceof NonExistentQueue || e instanceof QueueEmpty)
                throw new NotFoundException(e.message);

            throw e;
        }
    }
}
