import { IsNotEmpty } from 'class-validator';

export class MessageDto {
    @IsNotEmpty()
    contents: string;
}
