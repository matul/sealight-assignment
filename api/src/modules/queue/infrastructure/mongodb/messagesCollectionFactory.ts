import { ConfigService } from '@nestjs/config';
import { Collection } from 'mongodb';
import { MongoClient } from 'mongodb';

export async function messagesCollectionFactory(
    config: ConfigService,
    client: MongoClient,
) {
    const collectionName = 'messages';
    const db = client.db(config.get('DB_NAME'));
    const exists = await (
        await db.collections()
    ).some((collection) => collection.collectionName === collectionName);
    let collection: Collection;
    if (!exists) {
        collection = await db.createCollection(collectionName);
    } else {
        collection = db.collection(collectionName);
    }

    if (!(await collection.indexExists('createdAt_1'))) {
        await collection.createIndex(
            {
                createdAt: 1,
            },
            {
                name: 'createdAt_1',
            },
        );
    }

    return collection;
}
