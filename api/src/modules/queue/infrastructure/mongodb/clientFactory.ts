import { ConfigService } from '@nestjs/config';
import { MongoClient } from 'mongodb';

export async function clientFactory(config: ConfigService) {
    const client = new MongoClient(config.get('MONGO_URI'));
    await client.connect();

    return client;
}
