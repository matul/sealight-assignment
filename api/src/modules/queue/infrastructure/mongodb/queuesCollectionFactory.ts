import { ConfigService } from '@nestjs/config';
import { Collection, MongoClient } from 'mongodb';

export async function queuesCollectionFactory(
    config: ConfigService,
    client: MongoClient,
) {
    const collectionName = 'queues';
    const db = client.db(config.get('DB_NAME'));
    const exists = await (
        await db.collections()
    ).some((collection) => collection.collectionName === collectionName);
    let collection: Collection;
    if (!exists) {
        collection = await db.createCollection(collectionName);
    } else {
        collection = db.collection(collectionName);
    }

    return collection;
}
