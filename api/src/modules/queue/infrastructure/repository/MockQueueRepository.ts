import { Queue } from '../../domain/aggregate';
import { IQueueRepository } from '../../domain/repository/IQueueRepository';

// until there is mongodb version
export class MockQueueRepository implements IQueueRepository {
    getQueues(): Promise<Queue[]> {
        return Promise.resolve([Queue.create('example')]);
    }

    persistQueue(queue: Queue): Promise<void> {
        return Promise.resolve();
    }

    removeMessage(messageId: string): Promise<void> {
        return Promise.resolve();
    }

    deleteQueue(queueName: string): Promise<void> {
        return Promise.resolve();
    }
}
