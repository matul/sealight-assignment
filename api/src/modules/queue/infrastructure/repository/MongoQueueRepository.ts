import { ClientSession, Collection, MongoClient } from 'mongodb';
import { Queue } from '../../domain/aggregate';
import { IQueueRepository } from '../../domain/repository/IQueueRepository';
import { Message } from '../../domain/aggregate/Message';
import { Inject } from '@nestjs/common';

interface MessageDocument {
    _id: string;
    queueName: string;
    contents: string;
    createdAt: Date;
}

interface QueueDocument {
    _id: string;
}

export class MongoQueueRepository implements IQueueRepository {
    constructor(
        private client: MongoClient,
        @Inject('MESSAGES_COLLECTION')
        private messagesCollection: Collection<MessageDocument>,
        @Inject('QUEUES_COLLECTION')
        private queuesCollection: Collection<QueueDocument>,
    ) {}

    async getQueues(): Promise<Queue[]> {
        const queueDocs = await this.queuesCollection.find().toArray();
        const messageDocs = await this.messagesCollection.find().toArray();
        const messagesPerQueue = new Map<string, Message[]>();
        messageDocs.forEach((message) => {
            const currentMessages =
                messagesPerQueue.get(message.queueName) ?? [];
            currentMessages.push(this.mapMessage(message));
            messagesPerQueue.set(message.queueName, currentMessages);
        });

        const queues = queueDocs.map(({ _id }) => {
            const queue = Queue.create(_id);
            const queueMessages = messagesPerQueue.get(queue.name) ?? [];
            queueMessages.forEach((message) => queue.enqueue(message));

            return queue;
        });

        return queues;
    }

    private mapMessage({ _id, contents, createdAt }: MessageDocument) {
        return Message.createExisting(contents, _id, createdAt);
    }

    async persistQueue(queue: Queue): Promise<void> {
        const session = this.client.startSession();
        session.startTransaction();
        const existingQueue = await this.queuesCollection.findOne({
            _id: queue.name,
        });
        if (!existingQueue) {
            await this.queuesCollection.insertOne({
                _id: queue.name,
            });
        }

        if (queue.snapshot.length > 0) {
            await this.persistMessages(session, queue);
        }
        await session.commitTransaction();
    }

    private async persistMessages(session: ClientSession, queue: Queue) {
        const bulk = this.messagesCollection.initializeUnorderedBulkOp({
            session,
        });
        queue.snapshot
            .map((message) => ({ ...message, queueName: queue.name }))
            .forEach((message) =>
                bulk
                    .find({ _id: message.id })
                    .upsert()
                    .update({
                        $set: <MessageDocument>{
                            _id: message.id,
                            contents: message.contents,
                            createdAt: message.createdAt,
                            queueName: message.queueName,
                        },
                    }),
            );
        await bulk.execute();
    }

    async removeMessage(messageId: string): Promise<void> {
        await this.messagesCollection.deleteOne({
            _id: messageId,
        });
    }

    async deleteQueue(queueName: string): Promise<void> {
        await this.queuesCollection.deleteOne({
            _id: queueName,
        });
    }
}
