import { Inject, Module, OnModuleInit } from '@nestjs/common';
import { QueueController } from './queue/application/controller/queue.controller';
import { MockQueueRepository } from './queue/infrastructure/repository/MockQueueRepository';
import { QueueService } from './queue/domain/service/QueueService';
import { clientFactory } from './queue/infrastructure/mongodb/clientFactory';
import { ConfigModule, ConfigService } from '@nestjs/config';
import { MongoClient } from 'mongodb';
import { messagesCollectionFactory } from './queue/infrastructure/mongodb/messagesCollectionFactory';
import { queuesCollectionFactory } from './queue/infrastructure/mongodb/queuesCollectionFactory';
import { MongoQueueRepository } from './queue/infrastructure/repository/MongoQueueRepository';
import { IQueueRepository } from './queue/domain/repository/IQueueRepository';
import { Queue } from './queue/domain/aggregate';

@Module({
    imports: [ConfigModule.forRoot()],
    providers: [
        {
            provide: 'QUEUE_REPOSITORY',
            useClass: MongoQueueRepository,
        },
        {
            provide: MongoClient,
            useFactory: clientFactory,
            inject: [ConfigService],
        },
        {
            provide: 'QUEUES_COLLECTION',
            useFactory: queuesCollectionFactory,
            inject: [ConfigService, MongoClient],
        },
        {
            provide: 'MESSAGES_COLLECTION',
            useFactory: messagesCollectionFactory,
            inject: [ConfigService, MongoClient],
        },
        QueueService,
    ],
    controllers: [QueueController],
    exports: [],
})
export class QueueModule implements OnModuleInit {
    constructor(
        private queueService: QueueService,
        @Inject('QUEUE_REPOSITORY') private repo: IQueueRepository,
    ) {}
    async onModuleInit() {
        return await this.queueService.loadQueues();
    }
}
