import { Module } from '@nestjs/common';
import { QueueModule } from './modules/queue.module';

@Module({
    imports: [QueueModule],
    controllers: [],
    providers: [],
})
export class AppModule {}
