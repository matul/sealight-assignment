# Sealight Assignment
Michał Matulka

## Missing features

1. Missing some tests
    - End to end tests using controller
    - Domain service tests
    - Repository tests

## Before using

Install dependencies inside `api` using `npm install` command.

## Basic usage

To start a project trigger `docker-compose up` and it will listen on `http://localhost:3000`

## Create queue

Trigger `POST localhost:3000/queues` with a following body:

```json
{
    "name": "example"
}
```

## Delete queue

Trigger `DELETE localhost:3000/queues/example`

## Enqueue

Trigger `POST localhost:3000/queues/example/messages` with a following body:

```json
{
    "contents": "example content"
}
```

## Dequeue

Trigger `DELETE localhost:3000/queues/example/messages`

## Snapshot

Trigger `GET localhost:3000/queues/example/messages`